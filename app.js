var express=require('express'), http=require('http');
var app=express();

app.get('/',function(req, res){
  res.sendfile(__dirname + '/index.html');
});
app.use("/", express.static(__dirname));

//var server=require('http').createServer(app),
// io=require('socket.io').listen(server);
var server=http.createServer(app).listen(3000, function(){
  console.log('Express server listening on port ' + app.get('port'));
});;

io=require('socket.io').listen(server);
var mongoose=require('mongoose');

mongoose.connect('mongodb://localhost/chat',function(err){

if(err)
	console.log(err);
else
	console.log("connected to MongoDB");

});

var schema=mongoose.Schema({
nick:String,
msg:String,
created: {type:Date, default:Date.now}

});
var loginSchema=mongoose.Schema({
nick:String,
pass:String,
created: {type:Date, default:Date.now}

});
var db=mongoose.model('Message',schema);
var login=mongoose.model('Login',loginSchema);

var user={};



io.sockets.on('connection', function(socket){
/*
db.where('nick', 'ali').findOne(function(err,docs){
console.log("THIS IS CALLING FUNCTION");
console.log(docs);
if(err)
	throw err;
	socket.emit('load old msg',docs);
});*/

var query=db.find({});


query.sort('-created').limit(4).exec(function(err,docs){
if(err)
	throw err;
	socket.emit('load old msg',docs);
});

socket.on('send message', function(data,callback){
var msg=data.trim();

	if(msg.substring(0,3)==='/w ')
	{
	msg=msg.substring(3);
	var ind=msg.indexOf(' ');
	if(ind!==-1)
	{
	var name=msg.substring(0,ind)
	var msg=msg.substring(ind+1);
	if(name in user)
		user[name].emit('whisper',socket.nick+": "+msg);
	else
		callback("Enter valid user name");
	}
	else
	callback("Enter Message");
}
else if (msg.length==0)
	callback("Enter message");
else
	{
	var newMsg=new db({msg:msg,nick:socket.nick});
	newMsg.save(function(err) {
		if(err)
			throw err;
		
		io.sockets.emit('new message',{msg:msg, nick:socket.nick});
		});
		}
});

socket.on('login user', function(data, callback){

login.find({nick:data.nick},function(err,docs){

console.log("DATABASE QUERY");
console.log(docs);
if(docs.length>0)
callback(false);
else
{
var newUser=new login({pass:data.pass,nick:data.nick});
	newUser.save(function(err) {
		if(err)
			throw err;
		
	//	io.sockets.emit('new message',{msg:msg, nick:socket.nick});
		});
socket.nick=data.nick;
callback(true);
console.log("logging user");
user[socket.nick]=socket;

updateUser();
socket.emit('new message',{msg:" welcome to the WEB CHAT",nick:socket.nick});


}
//callback
});


//if(data in user)
//{
//callback(false)
//}
//else
//{
//}
//io.sockets.emit('new message',data);



})

socket.on('add user', function(data, callback){

login.find({nick:data.nick, pass:data.pass},function(err,docs){

console.log("LOGIN QUERY");
console.log(docs);
if(docs.length==0)
callback(false);
else
{
socket.nick=data.nick;
callback(true);
console.log("LOGGING user");
user[socket.nick]=socket;

updateUser();
socket.emit('new message',{msg:" welcome to the WEB CHAT",nick:socket.nick});
}
//io.sockets.emit('new message',data);

})

})


function updateUser(){

io.sockets.emit('usernames',Object.keys(user));

}

socket.on('disconnect',function(data){

if(!socket.nick)
		return;
	delete user[socket.nick];

		//user.splice(user.indexOf(socket.nick),1);
	updateUser();
io.sockets.emit('new message',{msg:'<span class="error">: left the chat</span>', nick:socket.nick});

});


});